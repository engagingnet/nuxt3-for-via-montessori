import type { NitroCtx, Feed } from "nuxt-module-feed";

import axios from 'axios'

export default defineNitroPlugin((nitroApp) => {

	nitroApp.hooks.hook("feed:generate", async ({ feed, options }: NitroCtx) => {
		await createTestFeed(feed);
	});

	async function createTestFeed(feed: Feed) {

		feed.options = {
			id: 'Via Montessori',
			title: 'Via Montessori',
			description: 'For today’s Casa dei Bambini',
			link: 'https://via-montessori.com',
			language: 'en',
			image: 'https://res.cloudinary.com/via-montessori/image/upload/via-montessori-logo.png',
			favicon: 'https://res.cloudinary.com/via-montessori/image/upload/favicon-32x32.png',
			generator: "https://github.com/tresko/nuxt-module-feed",
		};

		type Post = {
			content: string;
			description: string;
			image: string;
			link: string;
			pubDate: Date;
			title: string;
		};

		const activity_posts = await (axios.get ('http://127.0.0.1:8055/items/activities?filter[Status][_eq]=Open&sort=-Publish_Date&limit=10') );
		const activity_entries = activity_posts.data.data;

		const article_posts = await (axios.get ('http://127.0.0.1:8055/items/articles?filter[Status][_eq]=Open&sort=-Publish_Date&limit=10') );
		const article_entries = article_posts.data.data;

		let items = activity_entries.concat(article_entries);
		items.sort((a, b) => (a.Publish_Date < b.Publish_Date) ? 1 : -1);
		items.slice(0, 10);

		let entries = [];

		items.forEach(item => {

			let collection = "activities";
			if (item.Shards) {
				collection = "articles";
			}

			entries.push({
				pubDate: new Date( item.Publish_Date ),
				title: item.Title,
				guid: item.id,
				link: 'https://via-montessori.com/' + collection + '/' + item.Slug,
				description: item.Blurb,
				// author: ourAuthors,
				// content: item.Image.id,
				// categories: [ 'Life Skills activities' ],
				image: 'https://res.cloudinary.com/via-montessori/image/upload/if_w_gt_500,c_scale,w_500/q_auto:good,e_auto_color,e_auto_brightness,e_auto_contrast/' + item.Image
			})

		});

		const posts: Post[] = entries;

		posts.forEach((post) => {
			feed.addItem({
				image: post.image,
				pubDate: post.pubDate,
				description: post.description,
				link: post.link,
				title: post.title,
			});
		});
	}
});