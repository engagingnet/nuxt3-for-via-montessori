export function processImageURL(url, width) {
	const code = url.substring(url.lastIndexOf('/') + 1);
	const newURL = 'https://res.cloudinary.com/via-montessori/image/upload/if_w_gt_' + width + ',c_scale,w_' + width + '/q_auto:good,e_auto_color,e_auto_brightness,e_auto_contrast/' + code;
	return newURL;
}