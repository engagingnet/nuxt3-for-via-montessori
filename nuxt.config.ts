// https://nuxt.com/docs/api/configuration/nuxt-config

export default defineNuxtConfig({
	app: {

		head: {
			// titleTemplate: '%s | ',
			htmlAttrs: {
				lang: 'en',
			},
			meta: [
				{ charset: 'utf-8' },
				{ name: 'viewport', content: 'width=device-width,width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0' }, // ,minimal-ui
				{ name: 'HandheldFriendly', content: 'true' },
				{ name: 'MobileOptimized', content: '320' },
				{ hid: 'description', name: 'description', content: "Exploring Dr Maria Montessori’s way of nurturing our children." },
				{ name: 'msapplication-TileColor', content: '#27bfff' },
				{ name: 'theme-color', color: '#c9774f' },
			],
			script: [
				// Can alreaey see slowdown to load, and don't know how to invoke
				//     { src: '//cdn.jsdelivr.net/npm/balance-text@3.2.1/balancetext.min.js' }
			],
			link: [
				// { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
				{ rel: 'icon', type: 'image/x-icon', size: '32x32', href: '//res.cloudinary.com/via-montessori/image/upload/favicon-32x32.png' },
				{ rel: 'icon', type: 'image/png', size: '16x16', href: '//res.cloudinary.com/via-montessori/image/upload/favicon-16x16.png' },
				{ rel: 'apple-touch-icon', size: '180x180', href: '//res.cloudinary.com/via-montessori/image/upload/apple-touch-icon.png' },
				{ rel: 'manifest', size: '180x180', href: '/site.webmanifest' },
				{ rel: 'mask-icon', color: '#5bbad5', href: '/safari-pinned-tab.svg' },

				{ rel: 'stylesheet', type: 'text/css', href: '//cloud.typography.com/6106452/7803592/css/fonts.css' },

				{ rel: 'alternate', type: 'application/rss+xml', href: '/feed', title: 'RSS' },
				// { type: 'text/js', href: 'https://cdnjs.cloudflare.com/ajax/libs/balance-text/3.2.1/balancetext.min.js' },
			]
		},
	},

	build: {

		// This stops things from repeating after an icon
		transpile: [
			'tslib',
		]
	},

	css: [
		'@/assets/backgrounds.scss',
		'@/assets/main.scss',
		'@/assets/page-transition.css',
		'@/assets/tooltip.scss',
		'@/assets/typography.scss',
		'@fortawesome/fontawesome-svg-core/styles.css',
	],

	devServer: {

		port: 3004
	},

    devtools: {

        enabled: true
    },

	// ssr: false,

	publicRuntimeConfig: {
		API_BASE_URL: process.env.API_BASE_URL
	},

	modules: [

		'@nuxtjs/apollo',
		'@nuxt/devtools',
		'@nuxt/image-edge',
		'nuxt-module-feed',
		'@vueuse/nuxt',
	],

	apollo: {

		clients: {
			default: {
				httpEndpoint: 'https://directus.via-montessori.com/graphql'
			}
		},

	},

	feed: {
		sources: [
		{
			path: "/feed", // The route to your feed.
			type: "rss2", // Can be: rss2, atom1, json1
			cacheTime: 60 * 15, // How long should the feed be cached
		},
		]
	},

	vite: {

		css: {
			preprocessorOptions: {
				scss: {
					additionalData: '@import "@/assets/breakpoints.scss"; @import "@/assets/colors.scss"; @import "@/assets/fonts.scss";',
				}
			}
		}
	},
})
