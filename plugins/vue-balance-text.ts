// @/plugins/markdownit.ts
import VueBalanceText from 'vue-balance-text';

export default defineNuxtPlugin(({ vueApp }) => {
	vueApp.use(VueBalanceText)
});
