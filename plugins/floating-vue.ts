import { defineNuxtPlugin } from '#app'
import FloatingVue from 'floating-vue'
import 'floating-vue/dist/style.css'

export default defineNuxtPlugin(({ vueApp }) => {
	vueApp.use(FloatingVue, {
		themes: {
			'info-tooltip': {
				distance: 24,
				delay: { 
					show: 30000, 
					hide: 0 
				},
			},
		}
	})
})