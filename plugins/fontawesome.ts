import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { faArrowsAltH } from '@fortawesome/fontawesome-pro-solid'
import { faChevronCircleUp } from '@fortawesome/fontawesome-pro-solid'
import { faRssSquare } from '@fortawesome/fontawesome-pro-solid'
import { faTh } from '@fortawesome/fontawesome-pro-solid'

library.add(faArrowsAltH, faChevronCircleUp, faRssSquare, faTh)

// This is important, we are going to let Nuxt worry about the CSS
config.autoAddCss = false

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('fa', FontAwesomeIcon, {})
})
